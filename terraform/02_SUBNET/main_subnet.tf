data "aws_vpc" "my_vpc" {
  tags = {
    "Owner" = "Fajar"
    "ManagedBy" = "Terraform"
  }
}

resource "aws_subnet" "subnet_fajar_pub_tf" {
  vpc_id = "${data.aws_vpc.my_vpc.id}"
  cidr_block = "10.200.1.0/24"
  map_public_ip_on_launch = true
  tags = {
    "Name" = "subnet_pub_fajar_tf"
    "Owner" = "Fajar"
    "ManagedBy" = "Terraform"
    "Network" = "Public"
  }
}

resource "aws_subnet" "subnet_fajar_priv_tf" {
  vpc_id = "${data.aws_vpc.my_vpc.id}"
  cidr_block = "10.200.10.0/24"

  tags = {
    "Name" = "subnet_priv_fajar_tf"
    "Owner" = "Fajar"
    "ManagedBy" = "Terraform"
    "Network" = "Private"
  }
}