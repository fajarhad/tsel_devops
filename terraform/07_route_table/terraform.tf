terraform {
  required_providers {
      aws = {
      source = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
  backend "s3" {
    bucket = "bucket-fajarhad"
    key = "tsel_devops/07_fajar_route_table.tfstate"
    region = "ap-southeast-1"
  }
}

provider "aws" {
  region = "ap-southeast-1"
}