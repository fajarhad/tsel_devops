data "aws_vpc" "my_vpc" {
  tags = {
    "Owner" = "Fajar"
    "ManagedBy" = "Terraform"
  }
}

resource "aws_subnet" "subnet_fajar_pub_tf" {
  vpc_id = "${data.aws_vpc.my_vpc.id}"
  cidr_block = "10.200.1.0/24"

  tags = {
    "Name" = "subnet_fajar_tf"
    "Owner" = "Fajar"
    "ManagedBy" = "Terraform"
  }
}