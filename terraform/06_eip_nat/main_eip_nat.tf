data "aws_internet_gateway" "fajar_igw"{
  tags = {
    "Owner" = "Fajar"
    "ManagedBy" = "Terraform"
  }
}

data "aws_subnet" "subnet_fajar_pub_tf" {
  tags = {
    "Name" = "subnet_pub_fajar_tf"
    "Owner" = "Fajar"
    "ManagedBy" = "Terraform"
    "Network" = "Public"
  }
}

resource "aws_eip" "eip_fajar_tf" {
  vpc = true
  depends_on = [data.aws_internet_gateway.fajar_igw]

  tags = {
    "Name" = "eip_fajar_tf"
    "Owner" = "Fajar"
    "ManagedBy" = "Terraform"
  }
}

resource "aws_nat_gateway" "fajar_natgw" {
  allocation_id = "${aws_eip.eip_fajar_tf.id}"
  subnet_id = "${data.aws_subnet.subnet_fajar_pub_tf.id}"
  depends_on = [data.aws_internet_gateway.fajar_igw]

  tags = {
    "Name" = "natgw_fajar_tf"
    "Owner" = "Fajar"
    "ManagedBy" = "Terraform"
  }
}