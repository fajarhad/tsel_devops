terraform {
  required_providers {
      aws = {
      source = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
  backend "s3" {
    bucket = "bucket-fajarhad"
    key = "tsel_devops/06_eip_nat_fajar.tfstate"
    region = "ap-southeast-1"
  }
}

provider "aws" {
  region = "ap-southeast-1"
}