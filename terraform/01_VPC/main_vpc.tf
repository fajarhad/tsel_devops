resource "aws_vpc" "vpc_fajar_tf" {
  cidr_block = "10.200.0.0/16"
  enable_dns_hostnames = true
  enable_dns_support   = true
  
  tags = {
    "Name" = "vpc_fajar_tf"
    "Owner" = "Fajar"
    "ManagedBy" = "Terraform"
  }
}
