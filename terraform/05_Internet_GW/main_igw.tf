data "aws_vpc" "my_vpc" {
  tags = {
    "Owner" = "Fajar"
    "ManagedBy" = "Terraform"
  }
}

resource "aws_internet_gateway" "fajar_igw"{
  vpc_id = "${data.aws_vpc.my_vpc.id}"
  tags = {
    Name = "fajar_igw"
    "Owner" = "Fajar"
    "ManagedBy" = "Terraform"
  }
}
