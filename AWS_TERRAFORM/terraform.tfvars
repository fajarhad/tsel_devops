region = "ap-southeast-1"
vpc_cidr = "10.200.0.0/16"
cidr_subnet_pub = "10.200.1.0/24"
cidr_subnet_priv = "10.200.10.0/24"

the_tags = {
  Owner = "Fajar"
  ManagedBy = "Terraform"
}

/* ====================== Instance ====================== */
webserver_ami = "ami-0d058fe428540cd89" // ubuntu server 20
webserver_instance_type = "t2.micro"
ec2_keyname = "fajar_20210427"