output "ip_webserver_public" {
  value = "${aws_instance.web_server.public_ip}"
}