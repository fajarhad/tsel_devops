resource "aws_instance" "web_server" {
  ami = "${var.webserver_ami}"
  instance_type = "${var.webserver_instance_type}"
  subnet_id = "${aws_subnet.subnet_fajar_pub_tf.id}"
  /* vpc_security_group_ids = [ "${aws_vpc.vpc_fajar.security_group_id}" ] */
  security_groups = [ "${aws_security_group.sg_webserver.id}" ]
  key_name = "${var.ec2_keyname}"
  
  tags = merge("${var.the_tags}",{
    Name = "pub_webserver_tf"
    Usage = "webserver"
  })
}