resource "aws_vpc" "vpc_fajar" {
  cidr_block = "${var.vpc_cidr}"
  enable_dns_hostnames = true
  enable_dns_support   = true
  
  tags = merge("${var.the_tags}",
    {Name = "vpc_fajar_tf"}
  )
}