variable "region" {
  description = "AWS Regional"
  default = "ap-southeast-1"
}

variable "vpc_cidr" {
  description = "CIDR for VPC"
  default = ""
}

variable "cidr_subnet_pub" {
  description = "CIDR for public subnet"
  default = "10.200.1.0/24"
}

variable "cidr_subnet_priv" {
  description = "CIDR for private subnet"
  default = "10.200.10.0/24"
}

variable "the_tags" {
  type = object({
    Owner = string
    ManagedBy = string
  })
  default = {
    Owner = "Fajar"
    ManagedBy = "Terraform"
  }
}

variable "webserver_ami" {
  description = "ami ID for web server"
  default = "ami-0d058fe428540cd89"
}

variable "webserver_instance_type" {
  description = "ami instance type for web server"
  default = "t2.micro"
}

variable "ec2_keyname" {
  description = "keyname for instance ec2"
  default = "fajar_20210427"
}