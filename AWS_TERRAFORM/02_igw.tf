/* Internet gateway for the public subnet */
resource "aws_internet_gateway" "igw_fajar" {
  vpc_id = "${aws_vpc.vpc_fajar.id}"
  
  tags = merge("${var.the_tags}", {
    "Name" = "igw_fajar_tf"
  })
}