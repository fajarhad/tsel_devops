resource "aws_security_group" "default" {
  name        = "sg_fajar_default_tf"
  description = "Default security group to allow inbound/outbound from the VPC"
  vpc_id      = "${aws_vpc.vpc_fajar.id}"
  depends_on  = [aws_vpc.vpc_fajar]

  /* Inbound - open SSH traffic */
  ingress {
    from_port = "22"
    to_port   = "22"
    protocol  = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  
  /* Outbound - open all traffic */
  egress {
    from_port = "0"
    to_port   = "0"
    protocol  = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = merge("${var.the_tags}",
    {Name = "sg_fajar_default_tf"}
  )
}

resource "aws_security_group" "sg_private" {
  name        = "sg_fajar_priv_tf"
  description = "Security group for private subnet"
  vpc_id      = "${aws_vpc.vpc_fajar.id}"
  depends_on  = [aws_vpc.vpc_fajar]

  /* Inbound - open SSH traffic */
  ingress {
    from_port = "22"
    to_port   = "22"
    protocol  = "tcp"
    cidr_blocks = ["${var.cidr_subnet_pub}"]
  }
  
  /* Outbound - open all traffic */
  egress {
    from_port = "0"
    to_port   = "0"
    protocol  = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = merge("${var.the_tags}",
    {Name = "sg_fajar_private_tf"}
  )
}

resource "aws_security_group" "sg_webserver" {
  name        = "sg_webserver_tf"
  description = "Security group for webserver"
  vpc_id      = "${aws_vpc.vpc_fajar.id}"
  depends_on  = [aws_vpc.vpc_fajar]

  /* Inbound - open SSH traffic */
  ingress {
    from_port = "22"
    to_port   = "22"
    protocol  = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  /* Inbound - open http - 80 */
  ingress {
    from_port = "80"
    to_port   = "80"
    protocol  = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  
  /* Inbound - open https - 443 */
  ingress {
    from_port = "443"
    to_port   = "443"
    protocol  = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  /* Outbound - open all traffic */
  egress {
    from_port = "0"
    to_port   = "0"
    protocol  = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = merge("${var.the_tags}",
    {Name = "sg_webserver_tf"}
  )
}