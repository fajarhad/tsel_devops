/* --------------------  SUBNET  --------------------*/

/* Public Subnet */
resource "aws_subnet" "subnet_fajar_pub_tf" {
  vpc_id = "${aws_vpc.vpc_fajar.id}"
  cidr_block = "${var.cidr_subnet_pub}"
  map_public_ip_on_launch = true
  
  tags = merge("${var.the_tags}", {
    "Name" = "subnet_fajar_pub_tf",
    "Network" = "Public"
  })
}

/* Private Subnet */
resource "aws_subnet" "subnet_fajar_priv_tf" {
  vpc_id = "${aws_vpc.vpc_fajar.id}"
  cidr_block = "${var.cidr_subnet_priv}"

  tags = merge("${var.the_tags}", {
    "Name" = "subnet_fajar_priv_tf",
    "Network" = "Private"
  })
}