/* Create Routing table for public subnet */
resource "aws_route_table" "rt_public" {
  vpc_id = "${aws_vpc.vpc_fajar.id}"
  
  tags = merge("${var.the_tags}", {
    "Name" = "rt_fajar_pub_tf"
    "Network" = "Public"
  })
}

/* Create Routing table for private subnet */
resource "aws_route_table" "rt_private" {
  vpc_id = "${aws_vpc.vpc_fajar.id}"
  
  tags = merge("${var.the_tags}", {
    "Name" = "rt_fajar_priv_tf"
    "Network" = "Private"
  })
}

/* Route table associations */
resource "aws_route_table_association" "public" {
  subnet_id      = "${aws_subnet.subnet_fajar_pub_tf.id}"
  route_table_id = "${aws_route_table.rt_public.id}"
}

resource "aws_route_table_association" "private" {
  subnet_id      = "${aws_subnet.subnet_fajar_priv_tf.id}"
  route_table_id = "${aws_route_table.rt_private.id}"
}

/* --------------------  Route for Routing Tables  --------------------*/

/* RT public to IGW */
resource "aws_route" "public_igw" {
  route_table_id = "${aws_route_table.rt_public.id}"
  destination_cidr_block = "0.0.0.0/0"
  gateway_id = "${aws_internet_gateway.igw_fajar.id}"
}

/* RT private to NatW */
resource "aws_route" "priv_natgw" {
  route_table_id = "${aws_route_table.rt_private.id}"
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id = "${aws_nat_gateway.natgw.id}"
}