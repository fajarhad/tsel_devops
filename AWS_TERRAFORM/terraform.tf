terraform {
  required_providers {
      aws = {
      source = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
  backend "s3" {
    bucket = "bucket-fajarhad"
    key = "tsel_devops/terraform_complete_vpc.tfstate"
    region = "ap-southeast-1"
  }
}

provider "aws" {
  region = "${var.region}"
}