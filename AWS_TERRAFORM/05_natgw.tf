/* Elastic IP for NAT */
resource "aws_nat_gateway" "natgw" {
  allocation_id = "${aws_eip.eip_natgw.id}"
  subnet_id = "${aws_subnet.subnet_fajar_pub_tf.id}"
  depends_on = [
    aws_internet_gateway.igw_fajar
  ]
  
  tags = merge("${var.the_tags}", {
    "Name" = "natgw_fajar_tf"
  })
}