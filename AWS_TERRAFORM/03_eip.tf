/* Elastic IP for NAT */
resource "aws_eip" "eip_natgw" {
  vpc = true
  depends_on = [
    aws_internet_gateway.igw_fajar
  ]
  
  tags = merge("${var.the_tags}", {
    "Name" = "igw_fajar_tf"
    "Usage" = "Eip for NATGW"
  })
}
